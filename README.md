# Ejemplo Vue

Un proyecto en Vue.js, Vuetify y Axios.

## Dependencias

- NodeJs 8 o 9
- IDE Java con soporte para Maven

El proyecto se probó con Node.js 9 y las versiones usadas de vue y vuetify 
tenían bugs con respecto a la versión Node 10.

## Instrucciones 
Para obtener un estado equivalente al primer commit ejecutar los primeros tres 
pasos. El paso 4 nos dejará en el estado del segundo commit.

1. Desempacar el proyecto *Ciudades*, construirlo y ejecutarlo con Maven o con un 
IDE con soporte de Maven. Este proyecto el parte de servidor o backend para el
proyecto de Vue.

2. Instalar Vue y Vuetify. Otras versiones más recientes podrían funcionar.

        npm install -g vue-cli@2.9.3
        npm install -g vuetify@1.0.18

3. Crear el proyecto Vue. Este paso pedirá una serie de opciones. Seleccionar 
aquellas que dejarán el resultado que se muestra después del comando. El nombre
del proyecto y autor no son relevantes para el funcionamiento.

        vue init webpack ejemplo2

        ? Project name ejemplo2
        ? Project description A Vue.js project
        ? Author Tu nombre<direccion de correo>
        ? Vue build standalone
        ? Install vue-router? No
        ? Use ESLint to lint your code? No
        ? Set up unit tests No
        ? Setup e2e tests with Nightwatch? No
        ? Should we run `npm install` for you after the project has been created? (recommended) npm


4. Instalar dependencias adicionales

        cd ejemplo2
        npm install url-loader@1.0.1
        npm install --keep vuetify
        npm install --keep axios


5. Ejecutar el proyecto

        npm run dev
